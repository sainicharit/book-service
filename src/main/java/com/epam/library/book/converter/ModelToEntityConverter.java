package com.epam.library.book.converter;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.epam.library.book.model.Book;
import com.epam.library.book.model.BookAuthor;
import com.epam.library.book.model.BookCategory;
import com.epam.library.book.repository.entity.Author;
import com.epam.library.book.repository.entity.BookData;
import com.epam.library.book.repository.entity.Category;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * This class converts Models to Entities and vice-versa.
 * 
 * @author Charit_Saini
 *
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ModelToEntityConverter {

	public static BookData toEntity(Book book) {
		Set<Category> categories = null;
		if (null != book.getCategories()) {
			categories = book.getCategories().parallelStream().map(category -> {
				Category bookCategory = new Category();
				bookCategory.setCategory(category.getName());
				return bookCategory;
			}).collect(Collectors.toSet());

		}

		Set<Author> authors = null;
		if (null != book.getAuthors()) {
			authors = book.getAuthors().parallelStream().map(author -> {
				Author bookAuthor = new Author();
				bookAuthor.setName(author.getName());
				return bookAuthor;
			}).collect(Collectors.toSet());
		}

		BookData bookData = new BookData();
		bookData.setTitle(book.getTitle());
		bookData.setDescription(book.getDescription());
		bookData.setAuthors(authors);
		bookData.setCategories(categories);
		return bookData;
	}

	public static BookData toExistingEntity(BookData bookData, Book book) {
		Set<Category> categories = null;
		if (null != book.getCategories()) {
			categories = book.getCategories().parallelStream().map(category -> {
				Category bookCategory = new Category();
				bookCategory.setCategory(category.getName());
				return bookCategory;
			}).collect(Collectors.toSet());

		}

		Set<Author> authors = null;
		if (null != book.getAuthors()) {
			authors = book.getAuthors().parallelStream().map(author -> {
				Author bookAuthor = new Author();
				bookAuthor.setName(author.getName());
				return bookAuthor;
			}).collect(Collectors.toSet());
		}

		bookData.setDescription(book.getDescription());
		bookData.setAuthors(authors);
		bookData.setCategories(categories);
		return bookData;
	}

	public static Book toModel(BookData bookData) {
		List<BookAuthor> bookAuthors = null;
		if (null != bookData.getAuthors()) {
			bookAuthors = bookData.getAuthors().parallelStream().map(author -> {
				BookAuthor bookAuthor = new BookAuthor();
				bookAuthor.setAuthorId(author.getAuthorId());
				bookAuthor.setName(author.getName());
				return bookAuthor;
			}).collect(Collectors.toList());
		}

		List<BookCategory> bookCategories = null;
		if (null != bookData.getCategories()) {
			bookCategories = bookData.getCategories().parallelStream().map(category -> {
				BookCategory bookCategory = new BookCategory();
				bookCategory.setCategoryId(category.getCategoryId());
				bookCategory.setName(category.getCategory());
				return bookCategory;
			}).collect(Collectors.toList());
		}

		Book book = new Book();
		book.setBookId(bookData.getBookId());
		book.setTitle(bookData.getTitle());
		book.setDescription(bookData.getDescription());
		book.setAuthors(bookAuthors);
		book.setCategories(bookCategories);
		return book;
	}

}
