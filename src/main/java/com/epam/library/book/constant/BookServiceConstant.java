package com.epam.library.book.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BookServiceConstant {
	
	public static final String system_exception = "SYSTEM_EXCEPTION";
	public static final String valid_case_exception = "VALID_CASE_EXCEPTION";
	
	
}
