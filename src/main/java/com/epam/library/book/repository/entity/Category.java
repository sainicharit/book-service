package com.epam.library.book.repository.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Category {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenForCategory")
	@SequenceGenerator(initialValue = 1, name = "idGenForCategory")
	private Long categoryId;
	
	private String category;
	
	@ManyToMany(mappedBy = "categories")
	private Set<BookData> book;

}
