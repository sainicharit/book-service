package com.epam.library.book.repository.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class BookData {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenForBook")
	@SequenceGenerator(initialValue = 1, name = "idGenForBook")
	private Long bookId;
	
	@Column(unique = true)
	private String title;
	
	private String description;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "book_author", 
	joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "bookId"), 
	inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "authorId"))
	private Set<Author> authors;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "book_category", 
	joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "bookId"), 
	inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "categoryId"))
	private Set<Category> categories;

}
