package com.epam.library.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.library.book.repository.entity.BookData;

/**
 * Repository interface whose implementation interact with the database.
 * 
 * @author Charit_Saini
 *
 */
public interface BookRepository extends JpaRepository<BookData, Long>{

}
