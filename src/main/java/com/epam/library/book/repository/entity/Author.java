package com.epam.library.book.repository.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Author {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenForAuthor")
	@SequenceGenerator(initialValue = 1, name = "idGenForAuthor")
	private Long authorId;
	
	private String name;
	
	@ManyToMany(mappedBy = "authors")
	private Set<BookData> book;
	
}
