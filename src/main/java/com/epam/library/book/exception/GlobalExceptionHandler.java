package com.epam.library.book.exception;

import java.time.LocalDateTime;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.library.book.constant.BookServiceConstant;

@ControllerAdvice
@RequestMapping(produces = "application/json")
public class GlobalExceptionHandler {

	private static final String errorMessage = "System encountered with some internal error";

	@ExceptionHandler(RecordNotFoundException.class)
	public ResponseEntity<Error> handleRecordNotFoundexception(RecordNotFoundException ex) {
		Error errorDetails = new Error(false,LocalDateTime.now().toString(), HttpStatus.NOT_FOUND.value(),
				ex.getErrorMessage(), BookServiceConstant.valid_case_exception);
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(RecordNotModifiedException.class)
	public ResponseEntity<Error> handleRecordNotModifiedException(RecordNotModifiedException ex) {
		Error errorDetails = new Error(false, LocalDateTime.now().toString(), HttpStatus.BAD_REQUEST.value(),
				ex.getErrorMessage(), BookServiceConstant.valid_case_exception);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<Error> handleConstraintViolationException(ConstraintViolationException ex) {
		Error errorDetails = new Error(false, LocalDateTime.now().toString(), HttpStatus.BAD_REQUEST.value(), ex.getMessage(),
				BookServiceConstant.valid_case_exception);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Error> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
		Error errorDetails = new Error(false, LocalDateTime.now().toString(), HttpStatus.BAD_REQUEST.value(), ex.getMessage(),
				BookServiceConstant.valid_case_exception);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	/*
	 * @ExceptionHandler(Exception.class) public ResponseEntity<Error>
	 * handleException(Exception ex) { Error errorDetails = new
	 * Error(LocalDateTime.now().toString(),
	 * HttpStatus.INTERNAL_SERVER_ERROR.value(), errorMessage,
	 * BookServiceConstant.system_exception); return new
	 * ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR); }
	 */

}
