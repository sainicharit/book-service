package com.epam.library.book.exception;

import lombok.Getter;

@Getter
public class RecordNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1322258431416192194L;
	private String errorMessage;

	public RecordNotFoundException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}
	
}
