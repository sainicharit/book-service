package com.epam.library.book.exception;

import lombok.Getter;

@Getter
public class RecordNotModifiedException extends RuntimeException {
	
	private static final long serialVersionUID = 7049688679837882073L;
	private String errorMessage;

	public RecordNotModifiedException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}
	

}
