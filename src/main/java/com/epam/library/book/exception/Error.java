package com.epam.library.book.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Error {
	
	private boolean success;
	
	private String timeStamp;
	
	//This can also be application defined error code
	private int httpStatusCode;
	
	private String erroMessage;
	
	//Valid failure case or System/runtime error case
	private String type;
	

}
