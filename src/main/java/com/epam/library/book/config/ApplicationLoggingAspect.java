package com.epam.library.book.config;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ApplicationLoggingAspect {
	
	private Logger userLogger = Logger.getLogger(ApplicationLoggingAspect.class);

	@Before("execution(* com.epam.library.book..*.*(..))")
	public void logEntry(JoinPoint joinPoint) {
		String method = joinPoint.getSignature().toShortString();
		userLogger.info("********Entering : " + method);		
	}

	@After("execution(* com.epam.library.book..*.*(..))")
	public void logExit(JoinPoint joinPoint) {
		String method = joinPoint.getSignature().toShortString();
		userLogger.info("********Existing: " + method);
	}

	@AfterThrowing(pointcut = "execution(* com.epam.library.book..*.*(..))", throwing = "exec")
	public void logException(JoinPoint joinPoint, Exception exec) {
		String method = joinPoint.getSignature().toShortString();
		userLogger.error("Exception occured in :" + method + " : " + exec);
	}
}
