package com.epam.library.book.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.epam.library.book"))              
          .paths(PathSelectors.any())                          
          .build()
          .apiInfo(apiInfo())
          .useDefaultResponseMessages(false) ;
    }

	
	private ApiInfo apiInfo() {
	    return new ApiInfo(
	      "Book Service - REST API", 
	      "This service exposes endpoints that hanldes book specific CRUD operations.", 
	      "v1", 
	      "api/v1", 
	      new Contact("Charit Saini", "www.epam.com", "Charit_Saini@epam.com"), 
	      null ,null, Collections.emptyList());
	}
}
