package com.epam.library.book.model;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = Include.NON_NULL)
@ApiModel(description = "Describes all book attributes. None attribute cannot be null. ")
public class Book{
	
	@ApiModelProperty(notes = "Describes the book Id.")
	private Long bookId;
	
	@ApiModelProperty(notes = "Describes the book title.")
	private String title;
	
	@ApiModelProperty(notes = "Describes the book description.")
	private String description;
	
	@ApiModelProperty(notes = "Describes the book associated authors.")
	@NotEmpty
	private List<BookAuthor> authors;
	
	@ApiModelProperty(notes = "Describes the book associated categories.")
	@NotEmpty
	private List<BookCategory> categories;
	

}
