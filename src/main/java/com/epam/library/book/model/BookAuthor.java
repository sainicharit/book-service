package com.epam.library.book.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Describes all book author attributes. ")
public class BookAuthor {
	
	@ApiModelProperty(notes = "Describes the retrieved book author Id.")
	private Long authorId;
	
	@ApiModelProperty(notes = "Describes the retrieved book author name.")
	private String name;
	

}
