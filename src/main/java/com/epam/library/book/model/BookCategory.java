package com.epam.library.book.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Describes all book category attributes.")
public class BookCategory {
	
	@ApiModelProperty(notes = "Describes the retrieved book category Id.")
	private Long categoryId;
	
	@ApiModelProperty(notes = "Describes the retrieved book category Name.")
	private String name;

}
