package com.epam.library.book.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.epam.library.book.converter.ModelToEntityConverter;
import com.epam.library.book.exception.RecordNotFoundException;
import com.epam.library.book.exception.RecordNotModifiedException;
import com.epam.library.book.model.Book;
import com.epam.library.book.repository.BookRepository;
import com.epam.library.book.repository.entity.BookData;

@Service
public class BookServiceImpl implements BookService {

	private static final Logger LOGGER = Logger.getLogger(BookServiceImpl.class);

	@Autowired
	private BookRepository bookRepository;

	@Override
	public List<Book> getAllBooks() {
		LOGGER.info("**Going to get all the Book from the database.**");
		List<BookData> book = bookRepository.findAll();
		return book.parallelStream().map(ModelToEntityConverter::toModel).collect(Collectors.toList());
	}

	@Override
	public Book getBook(Long id) {
		LOGGER.info("**Going to get the Book from the database.**");
		BookData retrievedBook  = bookRepository.findById(id).orElseThrow(() -> {
			LOGGER.info("**No book found with the given id in the in the database.**");
			return new RecordNotFoundException("No record exist for given id!!");
		});
		return ModelToEntityConverter.toModel(retrievedBook);

	}

	@Override
	public Book addBook(Book book) {
		LOGGER.info("**Going to save Book in the database.**");
		try {
			BookData bookData = bookRepository.save(ModelToEntityConverter.toEntity(book));
			return ModelToEntityConverter.toModel(bookData);
		} catch (Exception e) {
			LOGGER.info("**Error occured while saving the book to the database.**", e);
			throw new RecordNotModifiedException(e.getMessage());
		}
	}

	@Override
	public Book updateBook(Long id, Book book) {
		LOGGER.info("**Going to update the Book in the database.**");
		BookData retrievedBook = bookRepository.findById(id).orElseThrow(() -> {
			LOGGER.info("**Error occured while updating the book to the database as book by Id does not exists.**");
			return new RecordNotFoundException("No record modified as it does not exist for given id");
		});
		BookData updatedBook = bookRepository.save(ModelToEntityConverter.toExistingEntity(retrievedBook, book));
		return ModelToEntityConverter.toModel(updatedBook);
	}

	@Override
	public void deleteBook(Long id) {
		LOGGER.info("**Going to delete the Book from the database.**");
		bookRepository.findById(id).orElseThrow(() -> {
			LOGGER.info("**Error occured while deleting the book to the database.**");
			throw new RecordNotModifiedException("No record modified as it does not exists.");
		});
		bookRepository.deleteById(id);
	}

}
