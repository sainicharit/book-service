package com.epam.library.book.service;

import java.util.List;

import com.epam.library.book.model.Book;

/**
 * Service interface that handles all the CRUD operation for Book controller.
 * 
 * @author Charit_Saini
 *
 */
public interface BookService {

	public List<Book> getAllBooks();
	
	public Book getBook(Long id);

	public Book addBook(Book book);

	public Book updateBook(Long id, Book book);

	public void deleteBook(Long id);

}
