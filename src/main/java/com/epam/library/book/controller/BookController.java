package com.epam.library.book.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.library.book.model.Book;
import com.epam.library.book.model.BookResponse;
import com.epam.library.book.model.BooksResponse;
import com.epam.library.book.service.BookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Rest Controller that exposes various operations related to Book for library
 * system.
 * 
 * @author Charit_Saini
 *
 */
@RestController
@Validated
@RequestMapping("api/v1/books")
@Api(description = "This Controller caters to the book specifc CRUD operrations.", protocols = "http")
public class BookController {

	@Autowired
	private BookService bookService;

	@ApiOperation(value = "View a list of all the available books.", response = BooksResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list from the system."),
			@ApiResponse(code = 404, message = "No books found.") })
	@GetMapping(produces = "application/json")
	public ResponseEntity<BooksResponse> getAllBooks() {
		List<Book> books = bookService.getAllBooks();
		BooksResponse response = new BooksResponse();
		response.setBooks(books);
		response.setSuccess(true);
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@ApiOperation(value = "View a specific book out of all the available Books from the system.", response = BookResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved Book."),
			@ApiResponse(code = 404, message = "No book found.") })
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<BookResponse> getBook(
			@ApiParam(value = "Book Id from which book will be retrieved", required = true) @PathVariable @NotNull @Positive Long id) {
		Book book = bookService.getBook(id);
		BookResponse response = new BookResponse();
		response.setBook(book);
		response.setSuccess(true);
		return new ResponseEntity<BookResponse>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@ApiOperation(value = "Add a book in the system.", response = BookResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully Saved the Book."),
			@ApiResponse(code = 400, message = "Bad request from the user.") })
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<BookResponse> addBook(@ApiParam(value = "Book object that will be saved.", required = true) @Valid @RequestBody Book book) {
		Book savedBook = bookService.addBook(book);
		BookResponse response = new BookResponse();
		response.setBook(savedBook);
		response.setSuccess(true);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedBook.getBookId()).toUri();
		return ResponseEntity.created(location).body(response);
	}

	@ApiOperation(value = "Update a book in the system.", response = BookResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 202, message = "Successfully Update the Book."),
			@ApiResponse(code = 404, message = "No book found for the updation.") })
	@PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<BookResponse> updateBook(
			@ApiParam(value = "Book Id from which book will be updated.", required = true) @PathVariable(required = true) @NotNull @Positive Long id,
			@ApiParam(value = "Book object that will be updated.", required = true) @Valid @RequestBody Book book) {
		Book updatedBook = bookService.updateBook(id, book);
		BookResponse response = new BookResponse();
		response.setBook(updatedBook);
		response.setSuccess(true);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}

	@ApiOperation(value = "Delete a book from the system.", response = BookResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully deleted the Book."),
			@ApiResponse(code = 400, message = "Bad request from the user.") })
	@DeleteMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<BookResponse> deleteBook(
			@ApiParam(value = "Book Id from which book will be deleted", required = true) @PathVariable(required = true) @NotNull @Positive Long id) {
		bookService.deleteBook(id);
		BookResponse response = new BookResponse();
		response.setSuccess(true);
		return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
	}

}
