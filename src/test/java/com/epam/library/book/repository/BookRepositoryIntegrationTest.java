package com.epam.library.book.repository;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.epam.library.book.repository.BookRepository;
import com.epam.library.book.repository.entity.Author;
import com.epam.library.book.repository.entity.BookData;
import com.epam.library.book.repository.entity.Category;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BookRepositoryIntegrationTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private BookRepository repository;
	
	@Before
	public void setup() {
		Set<Author> authors = new HashSet<>();
		Author author = new Author();
		author.setName("TestAuthor");
		authors.add(author);
		Set<Category> categories = new HashSet<>();
		Category category = new Category();
		category.setCategory("testcat");
		categories.add(category);
		BookData bookToPersist = new BookData();
		bookToPersist.setAuthors(authors);
		bookToPersist.setCategories(categories);
		bookToPersist.setTitle("testTitke");
		bookToPersist.setDescription("testDesc");
		
		entityManager.persistAndFlush(bookToPersist);
	}
	
	@Test
	public void testFindAll() {
		List<BookData> books = repository.findAll();	
		assertTrue(!CollectionUtils.isEmpty(books));	
	}
	
	@Test
	public void testSave() {
		Set<Author> authors = new HashSet<>();
		Author author = new Author();
		author.setName("TestAuthor");
		authors.add(author);
		Set<Category> categories = new HashSet<>();
		Category category = new Category();
		category.setCategory("testcat");
		categories.add(category);
		BookData bookToPersist = new BookData();
		bookToPersist.setAuthors(authors);
		bookToPersist.setCategories(categories);
		bookToPersist.setTitle("testTitkets2");
		bookToPersist.setDescription("testDesc");
		
		BookData savedBooked = repository.save(bookToPersist);
		
		assertTrue(null != savedBooked);	
	}

}
