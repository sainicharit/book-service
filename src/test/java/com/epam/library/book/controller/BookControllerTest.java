package com.epam.library.book.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.epam.library.book.model.Book;
import com.epam.library.book.model.BookAuthor;
import com.epam.library.book.model.BookCategory;
import com.epam.library.book.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)	
public class BookControllerTest {
	
	@MockBean
	private BookService service;
	
	@Autowired
    private MockMvc mvc;
	
	private static Book book;
	
	@BeforeClass
	public static void setup()
	{
		List<BookAuthor> authors = new ArrayList<>();
		BookAuthor author = new BookAuthor();
		author.setName("TestAuthor");
		authors.add(author);
		List<BookCategory> categories = new ArrayList<>();
		BookCategory category = new BookCategory();
		category.setName("testcat");
		categories.add(category);
		book = new Book();
		book.setAuthors(authors);
		book.setCategories(categories);
		book.setTitle("testTitke");
		book.setDescription("testDesc");
		
	}
	
	@Test
	public void testGetAllBook() {
		List<Book> books = new ArrayList<>();
		books.add(book);
		Mockito.when(service.getAllBooks()).thenReturn(books);
		
		try {
			mvc.perform(MockMvcRequestBuilders.get("/api/v1/books").contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Test
	public void testGetBook() {
		Mockito.when(service.getBook(Mockito.anyLong())).thenReturn(book);
		
		try {
			mvc.perform(MockMvcRequestBuilders.get("/api/v1/books/{id}",1)
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Test
	public void testAddBook() {
		Mockito.when(service.addBook(Mockito.anyObject())).thenReturn(book);
		
		try {
			mvc.perform(MockMvcRequestBuilders.post("/api/v1/books")
					.contentType(MediaType.APPLICATION_JSON)
					.content(asJsonString(book)))
			.andExpect(MockMvcResultMatchers.status().isCreated())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
	@Test
	public void testUpdateBook() {
		Mockito.when(service.updateBook(Mockito.anyLong(), Mockito.anyObject())).thenReturn(book);
		
		try {
			mvc.perform(MockMvcRequestBuilders.put("/api/v1/books/{id}",1)
					.contentType(MediaType.APPLICATION_JSON)
					.content(asJsonString(book)))
			.andExpect(MockMvcResultMatchers.status().isAccepted())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Test
	public void testDeleteBook() {
		Mockito.doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				 System.out.println("called with arguments: " + Arrays.toString(args));
				return null;
			}
		}).when(service).deleteBook(Mockito.anyLong());
		try {
			mvc.perform(MockMvcRequestBuilders.delete("/api/v1/books/{id}",1)
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.status().isNoContent())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
