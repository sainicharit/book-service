package com.epam.library.book.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.epam.library.book.BookServiceApplication;
import com.epam.library.book.model.Book;
import com.epam.library.book.model.BookAuthor;
import com.epam.library.book.model.BookCategory;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = BookServiceApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integration-test.properties")
public class BookControllerIntegrationTest {

	@Autowired
	private MockMvc mvc;

	private static Book book;

	@BeforeClass
	public static void setup() {
		List<BookAuthor> authors = new ArrayList<>();
		BookAuthor author = new BookAuthor();
		author.setName("TestAuthor");
		authors.add(author);
		List<BookCategory> categories = new ArrayList<>();
		BookCategory category = new BookCategory();
		category.setName("testcat");
		categories.add(category);
		book = new Book();
		book.setAuthors(authors);
		book.setCategories(categories);
		book.setTitle("testTitke");
		book.setDescription("testDesc");
	}

	@Test
	@Order(1)
	public void testAddBook() {
		try {
			mvc.perform(MockMvcRequestBuilders.post("/api/v1/books")
					.contentType(MediaType.APPLICATION_JSON)
					.content(asJsonString(book)))
			.andExpect(MockMvcResultMatchers.status().isCreated())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
	@Test
	public void testGetAllBook() {
		try {
			mvc.perform(MockMvcRequestBuilders.get("/api/v1/books").contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
		
	@Test
	public void testGetBook() {
		try {
			mvc.perform(MockMvcRequestBuilders.get("/api/v1/books/{id}",1)
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testUpdateBook() {	
		List<BookAuthor> authors = new ArrayList<>();
		BookAuthor author = new BookAuthor();
		author.setName("TestAuthor");
		authors.add(author);
		List<BookCategory> categories = new ArrayList<>();
		BookCategory category = new BookCategory();
		category.setName("testcat");
		categories.add(category);
		Book book2 = new Book();
		book2.setAuthors(authors);
		book2.setCategories(categories);
		book2.setDescription("testDesc");
		
		try {
			mvc.perform(MockMvcRequestBuilders.put("/api/v1/books/{id}",1)
					.contentType(MediaType.APPLICATION_JSON)
					.content(asJsonString(book2)))
			.andExpect(MockMvcResultMatchers.status().isAccepted())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Test
	public void testDeleteBook() {

		try {
			mvc.perform(MockMvcRequestBuilders.delete("/api/v1/books/{id}",1)
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.status().isNoContent())
			.andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
}
