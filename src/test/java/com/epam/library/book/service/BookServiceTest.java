package com.epam.library.book.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.epam.library.book.exception.RecordNotFoundException;
import com.epam.library.book.exception.RecordNotModifiedException;
import com.epam.library.book.model.Book;
import com.epam.library.book.model.BookAuthor;
import com.epam.library.book.model.BookCategory;
import com.epam.library.book.repository.BookRepository;
import com.epam.library.book.repository.entity.BookData;

@RunWith(SpringRunner.class)
public class BookServiceTest {

	@TestConfiguration
	static class BookServiceTestContextConfiguration {
		@Bean
        public BookService employeeService() {
			 return new BookServiceImpl();
		}
       
	}
	
	@Autowired
	private BookService service;
	
	@MockBean
	private BookRepository repository;
	
	private static Book book;
	
	@BeforeClass
	public static void setup()
	{
		List<BookAuthor> authors = new ArrayList<>();
		BookAuthor author = new BookAuthor();
		author.setName("TestAuthor");
		authors.add(author);
		List<BookCategory> categories = new ArrayList<>();
		BookCategory category = new BookCategory();
		category.setName("testcat");
		categories.add(category);
		book = new Book();
		book.setAuthors(authors);
		book.setCategories(categories);
		book.setTitle("testTitke");
		book.setDescription("testDesc");
		
	}
	
	@Test
	public void testGetAllBooksSuccessCase() {
		List<BookData> returnedBooks = new ArrayList<>();
		returnedBooks.add(new BookData());
		Mockito.when(repository.findAll()).thenReturn(returnedBooks);
		assertTrue(null != service.getAllBooks());		
	}
	
	@Test(expected = RecordNotFoundException.class)
	public void testGetAllBooksFailureCase() {
		Mockito.when(repository.findAll()).thenThrow(new RecordNotFoundException("File Not Found"));
		service.getAllBooks();
	}
	
	@Test
	public void testSaveBookSuccessCase() {
		Mockito.when(repository.save(Mockito.anyObject())).thenReturn(new BookData());
		assertTrue(null != service.addBook(book));		
	}
	
	@Test(expected = RecordNotModifiedException.class)
	public void testSaveBookFailureCAse() {
		Mockito.when(repository.save(Mockito.anyObject())).thenThrow(new RuntimeException("Exception occured!!"));
		service.addBook(book);
	}
	
	
	
}
